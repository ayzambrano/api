//version inicial
var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var express = require('express'),
 app = express(),
 port = process.env.PORT || 3010;

 var bodyParser = require('body-parser');
 app.use(bodyParser.json());
 app.use(function(req,res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
 });

var requestjeson=require('request-json');

var path = require('path');

var urlMovimientos ="https://api.mlab.com/api/1/databases/bdbanca3mb73914/collections/movimientos?apiKey=umAnD4rpga_7acS2Ksq7mb8iQBBpunqG";
var movimientosMLab = requestjeson.createClient(urlMovimientos);

var urlClientes ="https://api.mlab.com/api/1/databases/bdbanca3mb73914/collections/clientes?apiKey=umAnD4rpga_7acS2Ksq7mb8iQBBpunqG";
var clienteMLab = requestjeson.createClient(urlClientes);
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

/** API DE LOS MOVIMIENTOS **/
app.get('/',function(req, res){
   res.sendFile(path.join(__dirname,'index.html'));
});


//Obtiene todos los movimientos de un clientes
app.get('/movimientos/:idcliente',function(req,res){
  var jsonParam= {};
  jsonParam.idcliente = req.params.idcliente;
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlMovimientos+query;
  var ccMLabAux = requestjeson.createClient(urlAux);
  ccMLabAux.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});


  app.get('/movimientos/filtro/:idcliente&:tipocuenta&:tipomovimiento',function(req,res){
    var jsonParam= {};
    jsonParam.idcliente = req.params.idcliente;
    jsonParam.tipocuenta = req.params.tipocuenta;

    if(req.params.tipomovimiento!=0){
      jsonParam.tipomovimiento = req.params.tipomovimiento;
    }
    var strjsonParam = JSON.stringify(jsonParam);
    var query ="&q="+strjsonParam;
    var urlAux = urlMovimientos+query;
    var ccMLabAux = requestjeson.createClient(urlAux);
     ccMLabAux.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});
//Obtiene todos los movimientos de todos los clientes
app.get('/movimientos',function(req,res){
  movimientosMLab.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

app.post('/movimientos',function(req, res){
  movimientosMLab.post('',req.body,function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });

});



/** Clientes **/
app.get('/clientes/:idcliente',function(req,res){
  var jsonParam= {};
  jsonParam.idcliente = req.params.idcliente;
  console.log("entre a la opcion");
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlClientes+query;
  console.log(urlAux);
  var ccMLabAux = requestjeson.createClient(urlAux);
  ccMLabAux.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

app.get('/clientes/validarusuario/:usuario',function(req,res){
  var jsonParam= {};
  jsonParam.usuario = req.params.usuario;
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlClientes+query;
  var ccMLabAux = requestjeson.createClient(urlAux);
  ccMLabAux.get('', function(err,resM,body){
    if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

app.get('/clientes/login/:usuario&:password',function(req,res){
  var jsonParam= {};
  jsonParam.usuario = req.params.usuario;
  jsonParam.password = req.params.password;
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlClientes+query;
  var ccMLabAux = requestjeson.createClient(urlAux);
  console.log(urlAux);
  ccMLabAux.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

app.get('/clientes',function(req,res){
  clienteMLab.get('', function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});
app.post('/clientes',function(req, res){
  clienteMLab.post('',req.body,function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

//no se puede usar por las restricciones del API de mLab
app.put('/clientes/:usuario',function(req, res){
  var jsonParam= {};
  jsonParam.usuario = req.params.usuario;
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlClientes+query;
  var ccMLabAux = requestjeson.createClient(urlAux);
  console.log(urlAux);
  clienteMLab.put('',req.body,function(err,resM,body){
     if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});

//no se puede usar por las restricciones del API de mLab
app.delete('/clientes/:usuario',function(req, res){
  var jsonParam= {};
  jsonParam.usuario = req.params.usuario;
  var strjsonParam = JSON.stringify(jsonParam);
  var query ="&q="+strjsonParam;
  var urlAux = urlClientes+query;
  var ccMLabAux = requestjeson.createClient(urlAux);
  console.log(urlAux);
  ccMLabAux.delete('',function(err,resM,body){
    if(err){
        console.log(err);
    }else{
        res.send(body);
    }
  });
});
